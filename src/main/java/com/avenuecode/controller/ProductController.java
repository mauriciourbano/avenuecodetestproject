package com.avenuecode.controller;

import com.avenuecode.model.Product;
import com.avenuecode.model.view.ProductView;
import com.avenuecode.repository.ProductRepository;
import com.fasterxml.jackson.annotation.JsonView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by mauriciourbanofilho on 11/12/16.
 */
@RestController
@RequestMapping("/product")
public class ProductController extends BaseController {


    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Product>> getProducts() {
        List<Product> products = productRepository.findAll();
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Product> createProduct(@RequestBody Product product) {
        product = productRepository.save(product);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public ResponseEntity<Product> updateProduct(@RequestBody Product product) {
        product = productRepository.save(product);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id) {
        Product product = productRepository.findOne(id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}/childproducts")
    public ResponseEntity<List<Product>> getChildProductsById(@PathVariable Long id) {
        List<Product> products = productRepository.findProductsById(id);
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping("/with-child")
    @JsonView(ProductView.ProductAttributeView.class)
    public ResponseEntity<List<Product>> getProductsWithChildProducts() {
        List<Product> products = productRepository.findAll();
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping("/{id}/with-child")
    @JsonView(ProductView.ProductAttributeView.class)
    public ResponseEntity<Product> getProductByIdWithChildProducts(@PathVariable Long id) {
        Product product = productRepository.findOne(id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping("/with-images")
    @JsonView(ProductView.ImageAttributeView.class)
    public ResponseEntity<List<Product>> getProductsWithImages() {
        List<Product> products = productRepository.findAll();
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping("/{id}/with-images")
    @JsonView(ProductView.ImageAttributeView.class)
    public ResponseEntity<Product> getProductByIdWithImages(@PathVariable Long id) {
        Product product = productRepository.findOne(id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

    @RequestMapping("/simple")
    @JsonView(ProductView.SimpleProductView.class)
    public ResponseEntity<List<Product>> getProductsSimpleView() {
        List<Product> products = productRepository.findAll();
        return new ResponseEntity<List<Product>>(products, HttpStatus.OK);
    }

    @RequestMapping("/{id}/simple")
    @JsonView(ProductView.SimpleProductView.class)
    public ResponseEntity<Product> getProductByIdSimpleView(@PathVariable Long id) {
        Product product = productRepository.findOne(id);
        return new ResponseEntity<Product>(product, HttpStatus.OK);
    }

}
