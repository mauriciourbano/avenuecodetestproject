package com.avenuecode.repository;

import com.avenuecode.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by mauriciourbanofilho on 11/12/16.
 *
 * ProductRepository interface is used for CRUD operations
 */
@Repository
public interface ProductRepository extends JpaRepository<Product, Long>{

    @Query("SELECT p.products FROM Product p WHERE p.id = :id")
    List<Product> findProductsById(@Param("id") Long id);
}
