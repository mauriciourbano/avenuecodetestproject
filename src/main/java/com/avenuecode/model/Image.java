package com.avenuecode.model;

import com.avenuecode.model.view.ProductView;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by mauriciourbanofilho on 11/12/16.
 */
@Entity
public class Image implements Serializable {

    private static final long serialVersionUID = -7856065216395342200L;

    @Id
    @GeneratedValue
    @Column
    @JsonView(ProductView.ImageAttributeView.class)
    private Long id;

    @JsonIgnore
    @ManyToOne
    private Product product;

    @Column
    @JsonView(ProductView.ImageAttributeView.class)
    private String type;

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(final Product product) {
        this.product = product;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Image image = (Image) o;

        return id.equals(image.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}