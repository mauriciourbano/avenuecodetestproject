package com.avenuecode.model;

import com.avenuecode.model.view.ProductView;
import com.fasterxml.jackson.annotation.JsonView;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

/**
 * Created by mauriciourbanofilho on 11/12/16.
 */
@Entity
public class Product implements Serializable {

    private static final long serialVersionUID = -8923156790703001817L;

    @Id
    @GeneratedValue
    @Column
    @JsonView(ProductView.SimpleProductView.class)
    private Long id;

    @Column
    @JsonView(ProductView.SimpleProductView.class)
    private String name;

    @Column
    @JsonView(ProductView.SimpleProductView.class)
    private String description;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JsonView(ProductView.ProductAttributeView.class)
    private List<Product> products;

    @OneToMany(cascade = CascadeType.PERSIST)
    @JsonView(ProductView.ImageAttributeView.class)
    private List<Image> images;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return id.equals(product.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
