package com.avenuecode.model.view;

/**
 * Created by mauriciourbanofilho on 11/12/16.
 *
 * This class is used for the different representations of a Product.
 * There are three options:
 *     SimpleProductView: Does not include relationships
 *     ProductAttributeView: Includes child products
 *     ImageAttributeView: Include images
 */
public class ProductView {

    public static class SimpleProductView {}
    public static class ProductAttributeView extends SimpleProductView {}
    public static class ImageAttributeView extends SimpleProductView {}
}
