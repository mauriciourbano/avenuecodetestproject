package com.avenuecode.model.view;

import com.avenuecode.model.Image;
import com.avenuecode.model.Product;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.core.StringContains.containsString;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by mauriciourbanofilho on 11/12/16.
 */
public class ProductViewTest {

    private List<Product> products;
    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        createProductsList();
    }

    @Test
    public void whenUseSimpleProductView_thenDontSerializeRelationships() throws Exception {
        String json = mapper.writerWithView(ProductView.SimpleProductView.class)
                            .writeValueAsString(products);

        Assert.assertThat(json, not(containsString("products")));

    }

    @Test
    public void whenUseProductAttributeView_thenSerializeChildProducts() throws Exception {
        String json = mapper.writerWithView(ProductView.ProductAttributeView.class)
                .writeValueAsString(products);

        Assert.assertThat(json, containsString("products"));
        Assert.assertThat(json, not(containsString("images")));
    }

    @Test
    public void whenUseImageAttributeView_thenSerializeChildProducts() throws Exception {

        String json = mapper.writerWithView(ProductView.ImageAttributeView.class)
                .writeValueAsString(products);

        Assert.assertThat(json, containsString("images"));
        Assert.assertThat(json, not(containsString("products")));
    }

    @Test
    public void whenUseDefaultView_thenSerializeEverything() throws Exception {
        String json = mapper.writeValueAsString(products);

        Assert.assertThat(json, containsString("products"));
        Assert.assertThat(json, containsString("images"));
    }

    private void createProductsList() {
        Product product1 = new Product();
        product1.setId(1l);
        product1.setName("Product 1");
        product1.setDescription("Description for product 1");
        product1.setImages(Arrays.asList(new Image()));

        Product product2 = new Product();
        product2.setName("Product 2");
        product2.setDescription("Description for product 2");

        product1.setProducts(Arrays.asList(product2));
        products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
    }
}
