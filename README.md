# CRUD Product-Image #

This project perform CRUD operations on a Product resource using Image as a sub-resource of Product.

## Technical overview ##

* Maven project
* Spring boot
* H2 Database

In order to test the application, you should run the ProductViewTest class. It tests if the views created are returning the expected responses, which are: Products excluding relationships, products including child products and products including images.

## How to use the application? ##
You can test the application in two different ways:
### Maven tests ###
Just open a terminal window and go to the project folder. Type mvn test and all the tests are going to run automatically.

### Start the application ###
Just open a terminal window and go to the project folder. Type mvn spring-boot:run and a embedded server with the application is going to start on port 8080.

If you want to manually test the application, you can use a Rest Client of your own choice to send requests to the following endpoint:

* /product

Method: GET, POST and PUT

### Important info ###
- Since I had a short time to work on this project, I preferred to use Spring to create the web services and spring data to make the basic crud operations, because I believe it's much faster than pure Jersey.
- I also did not implement the delete operation and the web services for the image resource;
 
### Repository Owner ###

* Owner/admin: Urbano Filho, Mauricio (mauriciourbanofilho@gmail.com)